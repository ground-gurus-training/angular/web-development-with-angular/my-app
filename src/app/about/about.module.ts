import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutDetailsComponent } from './about-details/about-details.component';

@NgModule({
  declarations: [AboutDetailsComponent],
  imports: [CommonModule],
  exports: [AboutDetailsComponent],
})
export class AboutModule {}
