import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-greeter',
  templateUrl: './greeter.component.html',
  styleUrls: ['./greeter.component.scss'],
})
export class GreeterComponent {
  customer = {
    name: 'Duke',
    age: 33,
  };

  colors = ['Red', 'Orange', 'Blue', 'Green'];

  shockMe() {
    this.customer.name = 'Joe';
    this.customer.age = 30;
    console.log('Booooh!');
  }
}
